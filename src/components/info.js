// info.js
import React from 'react';
import ReactDOM from 'react-dom';

let Info = React.createClass({
	render: function() {
		return(
		<div className="row cells3">
			<div className="cell">
				<div className="panel">
					<div className="heading">
						<span className="title">React Starter</span>
					</div>
					<div className="content">
						<p>React Starter</p>
					</div>
				</div>
			</div>
			<div className="cell">
				<div className="panel alert">
					<div className="heading">
						<span className="title">Bowersify & Babelify</span>
					</div>
					<div className="content">
						<p>Uses Gulp with Browersify and Babelify to create a dev server.</p>
					</div>
				</div>
			</div>
			<div className="cell">
				<div className="panel success">
					<div className="heading">
						<span className="title">Dev Server</span>
					</div>
					<div className="content">
						<p>Uses simple server by Hapi to create a simple dev enviornment.</p>
					</div>
				</div>
			</div>            
		</div>
		);  
	}
});

ReactDOM.render(<Info/>, document.getElementById('layout-content'));