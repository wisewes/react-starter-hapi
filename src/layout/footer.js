// footer.js
import React from 'react';
import ReactDOM from 'react-dom';

let Footer = React.createClass({
  render: function() {
    return (
      <footer className="row ribbed-dark">
        <ul>
          <li>Footer Item 1</li>
          <li>Footer Item 2</li>
          <li>Footer item 3</li>
        </ul>
			</footer>
    );
  }
});  

ReactDOM.render(<Footer/>, document.getElementById('layout-footer'));

