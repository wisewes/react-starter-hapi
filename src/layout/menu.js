// menu.js
import React from 'react';
import ReactDOM from 'react-dom';

let Menu = React.createClass({
  render: function() {
    return (
      <header>
      <div className="app-bar">
        <a className="app-bar-element" href="...">Home</a>
        <span className="app-bar-divider"></span>
        <ul className="app-bar-menu">
            <li>   
                <a href="" className="dropdown-toggle">Items</a>
                <ul className="d-menu" data-role="dropdown">
                    <li><a href="">Menu 1</a></li>
                    <li><a href="">Menu 2</a></li>
                    <li><a href="">Menu 3</a></li>
                    <li><a href="">Menu 4</a></li>
                    <li className="divider"></li>
                    <li><a href="" className="dropdown-toggle">Menu 5</a>
                        <ul className="d-menu" data-role="dropdown">
                            <li><a href="">Submenu 1</a></li>
                            <li><a href="">Submenu 2</a></li>
                            <li><a href="">Submenu 3</a></li>
                        </ul>
                    </li>   
                </ul>
            </li>
            <li><a href="">Item 2</a></li>
            <li><a href="">Item 3</a></li>
        </ul>
    	</div>
      </header>     
    );
  }
});

ReactDOM.render(<Menu/>, document.getElementById('layout-menu'));

