/**
 * server.js
 * Simple HTTP server with Hapi
 */

var Hapi = require('hapi');
var Good = require('good');

var server = new Hapi.Server();

var options = {
	port: process.env.PORT || 4000,
	showListing: false
};

//connection
server.connection({
	host: '0.0.0.0',
	port: options.port
});

//register logging plugin
server.register({
	register: Good,
	options: {
		reporters: [{
			reporter: require('good-console'),	
			events: {
				response: '*',
				log: '*'
			}
		}]
	}
}, function(err) {
	if(err) { throw err; }
});

//register inert plugin for serving static files
server.register(require('inert'), function(err) {
	if(err) { throw err; }
	
	//serve files in /public 
	server.route({
		method: 'GET',
		path: '/{param*}',
		handler: {
			directory: {
				path: 'public',
				listing: options.showListing
			}
		}
	});
	
	//bower components
	server.route({
		method: 'GET',
		path: '/bower_components/{path*}',
		handler: { 
			directory: { 
				path: './bower_components',
				listing: true
			} 
		}
	});
	
	//build path
	server.route({
		method: 'GET',
		path: '/build/{path*}',
		handler: {
			directory: {
				path: './build'
			}
		}
	});
	
	//basic server info route
	server.route({
		method: 'GET',
		path: '/dev-server-info',
		handler: function(req, reply) {
			return reply(server.info);
		}
	});
});

//run it
server.start(function() {
	server.log('info', 'Server running at ' + server.info.uri);
});
